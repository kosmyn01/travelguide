package com.example.travelguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;

public class PlaceDetails extends AppCompatActivity {
    private static final String TAG = PlaceDetails.class.getSimpleName();

    private TextView placeName, placePhoneNumber, placeAddress, placeWebsite;
    private ImageView placePhoto;
    private Button buttonMaps;

    private String mCurrentLatitude, mCurrentLongitude, mCurrentPlaceName, mPlaceId;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);

        Intent intent = getIntent();
        mPlaceId = intent.getStringExtra(PlacesActivity.PLACEID);

        placeName = (TextView) findViewById(R.id.place_name);
        placePhoneNumber = (TextView) findViewById(R.id.place_phone_number);
        placeAddress = (TextView) findViewById(R.id.place_address);
        placeWebsite = (TextView) findViewById(R.id.place_website);
        placePhoto = (ImageView) findViewById(R.id.place_photo);

        buttonMaps = (Button) findViewById(R.id.button_maps);
        buttonMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsMarkerActivity.class);
                intent.putExtra(SearchActivity.LATITUDE, mCurrentLatitude);
                intent.putExtra(SearchActivity.LONGITUDE, mCurrentLongitude);
                intent.putExtra(SearchActivity.PLACE_NAME, mCurrentPlaceName);
                startActivity(intent);
            }
        });

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG, "onConnectionFailed()");
                    }
                })
                .build();

        mGoogleApiClient.connect();

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, mPlaceId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            final Place myPlace = places.get(0);
                            placeName.setText(myPlace.getName());
                            placeAddress.append(myPlace.getAddress());
                            placePhoneNumber.append(myPlace.getPhoneNumber());
                            placeWebsite.append(String.valueOf(myPlace.getWebsiteUri()));
                            mCurrentLatitude = String.valueOf(myPlace.getLatLng().latitude);
                            mCurrentLongitude = String.valueOf(myPlace.getLatLng().longitude);
                            mCurrentPlaceName = String.valueOf(myPlace.getName());

                            Log.i(TAG, "Place found: " + myPlace.getName());
                        } else {
                            Log.e(TAG, "Place not found");
                        }
                        places.release();
                    }
                });

        placePhotosAsync(mPlaceId);

    }

    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
            = new ResultCallback<PlacePhotoResult>() {
        @Override
        public void onResult(PlacePhotoResult placePhotoResult) {
            if (!placePhotoResult.getStatus().isSuccess()) {
                return;
            }
            placePhoto.setImageBitmap(placePhotoResult.getBitmap());
        }
    };

    /**
     * Load a bitmap from the photos API asynchronously
     * by using buffers and result callbacks.
     */
    private void placePhotosAsync(String placeId) {
        Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {


                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }

                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the first bitmap in an ImageView in the size of the view
                            photoMetadataBuffer.get(0)
                                    .getScaledPhoto(mGoogleApiClient, 750,
                                            750)
                                    .setResultCallback(mDisplayPhotoResultCallback);
                        }
                        photoMetadataBuffer.release();
                    }
                });
    }
}
