package com.example.travelguide;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String CATEGORY_NAME = "category_name";
    public static final String LATITUDE = "mLatitude";
    public static final String LONGITUDE = "longitude";
    private ListView listView;
    private String[] categories = {"Art Gallery", "Aquarium", "City Hall", "Church", "Library",
            "Museum", "Shopping Mall", "Park", "University", "Zoo"};

    private String mLatitude = null;
    private String mLongtitude = null;
    private Boolean gpsStatus = false;
    private LocationManager locationMgr = null;
    SharedPreferences sharedpreferences;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private TextView userTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        userTextView = (TextView) findViewById(R.id.user_text_view);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.row_layout, R.id.listText, categories);

        // Adding data into listview
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), PlacesActivity.class);
                intent.putExtra(CATEGORY_NAME, itemValue);
                intent.putExtra(LATITUDE, mLatitude);
                intent.putExtra(LONGITUDE, mLongtitude);
                startActivity(intent);
            }
        });

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;

            locationMgr = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            gpsStatus = displayGpsStatus();
            if (gpsStatus) {
                locationMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 5, this);
            } else {
                askUserToOpenGPS();
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        mLatitude = "47.1560519";
        mLongtitude = "27.4468857";

        sharedpreferences = getSharedPreferences(LoginActivity.MyPREFERENCES,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(LoginActivity.usernameKey)) {
            userTextView.setVisibility(View.VISIBLE);
            userTextView.setText("Hello, " +  sharedpreferences.getString(LoginActivity.usernameKey, "usernameKey"));
        }
//            switch (sharedpreferences.getString(userTypeKey, "userTypeKey")){
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v(TAG, String.format("Location changed: Long:%f,Lat:%f", (float) location.getLongitude(), (float) location.getLatitude()));

        mLatitude = String.valueOf(location.getLatitude());
        mLongtitude = String.valueOf(location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.v(TAG, "Status Changed");
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.v(TAG, String.format("Provider enabled: %s", s));
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.v(TAG, String.format("Provider disabled: %s", s));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        if (sharedpreferences.contains(LoginActivity.usernameKey)) {
            menu.findItem(R.id.logout).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.search:
                startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                return true;
            case R.id.maps:
                Intent intent = new Intent(getApplicationContext(), TouristSpotsOnMaps.class);
                intent.putExtra(LATITUDE, mLatitude);
                intent.putExtra(LONGITUDE, mLongtitude);
                startActivity(intent);
                return true;
            case R.id.settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            case R.id.logout:
                item.setVisible(false);
                userTextView.setVisibility(View.GONE);
                SharedPreferences sharedpreferences = getSharedPreferences
                        (LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    public void askUserToOpenGPS() {
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(this);
        mAlertDialog.setTitle("Locatie negasita, Deschideti GPS")
                .setMessage("Activati GPS pentru a determina locatia?")
                .setPositiveButton("Deschide setari", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Iesire", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;

        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    locationMgr = (LocationManager)
                            getSystemService(Context.LOCATION_SERVICE);
                    gpsStatus = displayGpsStatus();
                    if (gpsStatus) {
                        locationMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 5, this);
                    } else {
                        askUserToOpenGPS();
                    }
                }
            }
        }
        if(!mLocationPermissionGranted) {
            finish();
            System.exit(0);
        }
    }

}
