<?php

/**
 * A class file to connect to database
 */
class DATABASE_CONNECT {

    public $dbcon;
    function __construct() {
        $this->connect();
    }

    // destructor
    function __destruct() {
        $this->close();
    }

    function connect() {

        require_once __DIR__ . '/database_config.php';

        $con = mysqli_connect(DATABASE_SERVER_NAME, DATABASE_USER, DATABASE_PASSWORD) or die(mysqli_error());

        $db = mysqli_select_db($con, DATABASE_DATABASE_NAME) or die(mysqli_error()) or die(mysqli_error());

        $this->dbcon = $con;

        return $con;
    }

    function close() {
        mysqli_close($this->dbcon);
    }

}

?>